---
layout: default
---
<div class="container">
	<div class="section">
		<div class="header">
			<h1>llsif</h1>
			<p>Get your idol fix now! - rooted flavour ;)</p>
		</div>
	</div>
	<div class="section row">
		{% for dl in site.data.download %}
		<div class="col s8 offset-s2 m4">
			<a class="waves-effect waves-red btn modal-trigger" href="#{{ dl.ids }}">{{ dl.rel }} {{ dl.ver }}</a>
		</div>
		{% endfor %} {% for dl in site.data.download %}
		<div id="{{ dl.ids }}" class="modal bottom-sheet">
			<div class="modal-content">
				<h4>Choose Download</h4>
				<p>
					{{ dl.apk }}-v{{ dl.ver }}-signed.apk
					<br>
					<div class="chip">Released on {{ dl.dat }}</div>
				</p>
				<div class="row">
					<div class="col s6 m2 l2">
						<div class="card hoverable">
							<div class="card-image waves-effect waves-light">
								<a href="{{ site.dl.honoka }}{{ dl.apk }}-v{{ dl.ver }}-signed.apk">
									<img src="https://ik.imagekit.io/arc/honoka/0<?php $random = rand(31,40); echo $random; ?>.png" alt="honoka icon" />
								</a>
							</div>
						</div>
					</div>
					<div class="col s6 m2 l2">
						<div class="card hoverable">
							<div class="card-image waves-effect waves-light">
								<a href="{{ site.dl.hanayo }}{{ dl.apk }}-v{{ dl.ver }}-signed.apk">
									<img src="https://ik.imagekit.io/arc/hanayo/0<?php $random = rand(21,30); echo $random; ?>.png" alt="hanayo icon" />
								</a>
							</div>
						</div>
					</div>
					<div class="col s6 m2 l2">
						<div class="card hoverable">
							<div class="card-image waves-effect waves-light">
								<a href="{{ site.dl.kotori }}{{ dl.apk }}-v{{ dl.ver }}-signed.apk">
									<img src="https://ik.imagekit.io/arc/kotori/0<?php $random = rand(41,50); echo $random; ?>.png" alt="kotori icon" />
								</a>
							</div>
						</div>
					</div>
					<div class="col s6 m2 l2">
						<div class="card hoverable">
							<div class="card-image waves-effect waves-light">
								<a href="{{ site.dl.nozomi }}{{ dl.apk }}-v{{ dl.ver }}-signed.apk">
									<img src="https://ik.imagekit.io/arc/nozomi/0<?php $random = rand(71,80); echo $random; ?>.png" alt="nozomi icon" />
								</a>
							</div>
						</div>
					</div>
					<div class="col s6 m2 l2">
						<div class="card hoverable">
							<div class="card-image waves-effect waves-light">
								<a href="{{ site.dl.maki }}{{ dl.apk }}-v{{ dl.ver }}-signed.apk">
									<img src="https://ik.imagekit.io/arc/maki/0<?php $random = rand(51,60); echo $random; ?>.png" alt="maki icon" />
								</a>
							</div>
						</div>
					</div>
					<div class="col s6 m2 l2">
						<div class="card hoverable">
							<div class="card-image waves-effect waves-light">
								<a href="{{ site.dl.nico }}{{ dl.apk }}-v{{ dl.ver }}-signed.apk">
									<img src="https://ik.imagekit.io/arc/nico/0<?php $random = rand(61,70); echo $random; ?>.png" alt="nico icon" />
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="fixed-action-btn click-to-toggle" style="bottom: 10px; right: 10px;">
				<a class="btn-floating grey darken-1 waves-effect waves-light btn-flat">
					<i class="large material-icons">file_download</i>
				</a>
				<ul>
					<li>
						<a class="modal-action modal-close btn-floating black waves-effect waves-light">
							<i class="material-icons">close</i>
						</a>
					</li>
					<li>
						<a class="btn-floating teal accent-4 tooltipped waves-effect waves-light" href="{{ dl.play }}" target="_blank" data-position="left"
						  data-delay="50" data-tooltip="Google Play Store">
							<i class="material-icons">play_arrow</i>
						</a>
					</li>
					<li>
						<a class="btn-floating blue lighten-1 tooltipped waves-effect waves-light" href="{{ dl.qapp }}" target="_blank" data-position="left"
						  data-delay="50" data-tooltip="QooApp">
							<i class="material-icons">android</i>
						</a>
					</li>
				</ul>
			</div>
		</div>
		{% endfor %}
	</div>
</div>